'use strict'

angular.module('twiportr.services',[]);

angular.module('twiportr.services')
		.factory('authService', authService)
		.factory('twitterService', twitterService)
		

.factory('Twipost',
		['$resource','API_ENDPOINT',
		 function($resource,API_ENDPOINT)
		 {    return $resource(API_ENDPOINT, { id: '@id' });
		 }]);

authService.$inject = ['AUTH_ENDPOINT','REGISTER_ENDPOINT', 'LOGOUT_ENDPOINT',
                                 '$http', '$cookieStore', '$rootScope', '$q', '$timeout', 
                                  '$location'];

twitterService.$inject = ['CONNECT_ENDPOINT', '$http', '$cookieStore', '$rootScope', '$q', '$timeout', 
                                  '$location']


function twitterService(CONNECT_ENDPOINT, $http, $cookieStore, $rootScope, $q, $timeout, $location){
	
	var twitterService = {}
	
	var headerConfig = {headers:  {
        
        'Accept': 'application/json;odata=verbose',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Content-Range, Content-Disposition, Content-Description'
    }
};
	
	twitterService.importTweets = importTweets;
	
	return twitterService;
	
	function importTweets(callback){
	
		(function callback(){
		success = true;
		
		if(success){
		
			data = true
			
			
		}
		else{
			
		}
		callback && callback(data);
	}).error(function() {
		
		callback && callback(false);
	});

}
		
	
//		function handleSuccess(data) {
//		    
//			connectResult = true;
//			
//			return data;
//		}
//
//		function handleError(error) {
//		    return function () {
//		        return { success: false, message: error };
//		    };
//		}
	
	}



function authService(AUTH_ENDPOINT,REGISTER_ENDPOINT, LOGOUT_ENDPOINT, 
		$http, $cookieStore, $rootScope, $q, $timeout, $location) {

var service = {};


service.Login = Login;
service.SetAuthToken = SetAuthToken;
service.ClearAuthToken = ClearAuthToken;
service.Register = Register

return service;

function Login(username, password, callback) {
	
	$http.post(AUTH_ENDPOINT, {'username':username, 'password':password}).success(function(authenticationResult) {
		
		
		console.log(authenticationResult.token)
		
		if (authenticationResult) {
			
        	var authToken = authenticationResult.token;
        	$rootScope.authToken = authToken;
			$rootScope.authenticated = true;
			
			
			SetAuthToken(authToken);
			
			
		} else {
			
			$rootScope.authenticated = false;
		
		}
		callback && callback($rootScope.authenticated);
	}).error(function() {
		$rootScope.authenticated = false;
		callback && callback(false);
	});

}

function SetAuthToken(authToken) {
	$cookieStore.put('authToken', authToken);
}

function ClearAuthToken() {
    $rootScope.authToken = {};
    $cookieStore.remove('authToken');            
}


function Register(user) {
    return $http.post(REGISTER_ENDPOINT, user).then(handleSuccess, handleError('Error creating user'));
}

// private functions

function handleSuccess(data) {
    return data;
}

function handleError(error) {
    return function () {
        return { success: false, message: error };
    };
}

}



angular.module('twiportr.services').value('CONNECT_ENDPOINT','http://localhost:8080/connect/twitter&callback=JSON_CALLBACK');
angular.module('twiportr.services').value('API_ENDPOINT','http://localhost:3000/twiposts');
angular.module('twiportr.services').value('AUTH_ENDPOINT','http://localhost:8080/api/login');
angular.module('twiportr.services').value('LOGOUT_ENDPOINT','http://localhost:8080/api/logout'); 
angular.module('twiportr.services').value('REGISTER_ENDPOINT','http://localhost:8080/api/register'); 
