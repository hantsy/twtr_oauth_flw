(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    function RegisterController(UserService, $location, $rootScope, FlashService) {
        var vm = this;

        vm.register = register;

        function register() {
            vm.dataLoading = true;
            UserService.Create(vm.user)
                .then(function (resource) {
                    if (resource) {
                        FlashService.Success('Registration successful', true);
                        
                        $rootScope.username = resource.username;
                        
                        $location.path('/login');
                    } else {
                        FlashService.Error("Registration failure!");
                        vm.dataLoading = false;
                    }
                });
        }
    }

})();