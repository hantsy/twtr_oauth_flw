<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <title>dolatr</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css"> <!-- load bootstrap css -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body 		{ padding-top:80px; }
    </style>
</head>

<body>


<div class="container">

    <div class="col-sm-6 col-sm-offset-3">

        <h1><span class="fa fa-sign-in"></span> Signup</h1>
       
        <!-- LOGIN FORM -->
        
        <form:form class="form-group form" name="input" method="post" action="signup" modelAttribute="user">
		<form:errors path="username" />
		Username <form:input class="form-control" type="text" path="username" /><br/>
		<form:errors path="password" />
		Password <form:input class="form-control" type="text" path="password" /><br/>
		<form:errors path="email" />
		Email  <form:input class="form-control" type="text" path="email" /><br/>
		
		
		<span><button type="submit" class="btn btn-warning btn-lg">Signup</button></span>
		
		</form:form>
		
		
        <hr>

        <p>Already have an account? <a href="login">Login</a></p>
        

    </div>

</div>
</body>
</html>