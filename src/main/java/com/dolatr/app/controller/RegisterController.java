package com.dolatr.app.controller;

import javax.inject.Inject;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.documents.UserAccount;

@ComponentScan
@Controller
public class RegisterController {
	
	 @Inject
	 private UserAccountService userService;
	 	 
	 @RequestMapping(value = "/api/register", method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public ResponseEntity<UserAccount> register(@RequestBody UserAccount user) {
		
		 user = userService.register(user);
		
		 return new ResponseEntity<UserAccount>(user, HttpStatus.OK);
		
	}
	
	
}
