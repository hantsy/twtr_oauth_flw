package com.dolatr.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.resources.IndexResource;
import com.dolatr.app.resources.UserProfileResource;
import com.dolatr.app.resources.assemblers.IndexResourceAssembler;
import com.dolatr.app.resources.assemblers.UserProfileResourceAssembler;


@RestController
public class IndexController {

   private final IndexResourceAssembler indexResourceAssembler;
   private UserAccountRepository userAccountRepository;
   private UserProfileResourceAssembler userProfileResourceAssembler;
   
   
   @Autowired
   public IndexController(IndexResourceAssembler indexResourceAssembler, UserAccountRepository userAccountRepository, UserProfileResourceAssembler userProfileResourceAssembler) {
      this.indexResourceAssembler = indexResourceAssembler;
      this.userAccountRepository = userAccountRepository;
      this.userProfileResourceAssembler = userProfileResourceAssembler;
   }


   /**
    * Get public website resource root, with links to other resources
    * @return
    */
   @RequestMapping(method=RequestMethod.GET,  value = "/api")
   public ResponseEntity<IndexResource> getPublicWebsiteResource() {
      return new ResponseEntity<>(indexResourceAssembler.toResource(), HttpStatus.OK);
   }
   
   /**
    * Returns signed up user public profile info.
    * @param userId 
    * 
    * @param userId
    * @return
    */
   @RequestMapping(method = RequestMethod.GET, value = "/account/profile")
   public HttpEntity<UserProfileResource> getUserProfileByUserId(String userId) {
   	
   	 UserAccount userAccount = userAccountRepository.findByUserId(userId);
   	        
        UserProfileResource resource = userProfileResourceAssembler.toResource(userAccount);
       
       return new ResponseEntity<>(resource, HttpStatus.OK);
   
   }
   

   
   
}
